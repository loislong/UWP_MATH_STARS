﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace projet_UWP
{
    public class Jeu : INotifyPropertyChanged
    {
        private int terme1;
        private int terme2;
        private int resultat1;
        private int resultat2;
        private bool bonneReponseAGauche;

        private Operation operationActuelle;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public int Terme1
        {
            get { return this.terme1; }
            set
            {
                this.terme1 = value;
                this.OnPropertyChanged();
            }
        }
        public int Terme2
        {
            get { return this.terme2; }
            set
            {
                this.terme2 = value;
                this.OnPropertyChanged();
            }
        }
        public Operation OperationActuelle
        {
            get { return this.operationActuelle; }
            set
            {
                this.operationActuelle = value;
                this.OnPropertyChanged();

            }
        }
        public int Resultat1
        {
            get { return this.resultat1; }
            set
            {
                this.resultat1 = value;
                this.OnPropertyChanged();

            }
        }
        public int Resultat2
        {
            get { return this.resultat2; }
            set
            {
                this.resultat2 = value;
                this.OnPropertyChanged();

            }
        }

        public bool BonneReponseAGauche { get => bonneReponseAGauche; set => bonneReponseAGauche = value; }

        public Jeu()
        {
            Terme1 = 0;
            Terme2 = 0;
            OperationActuelle = RandomEnumValue<Operation>();
        }

        public void GenererOperation()
        {
            OperationActuelle = RandomEnumValue<Operation>();
        }

        public void GenererResultats()
        {
            Random ran = new Random();
            if (ran.Next(0, 2) == 0)
            {
                BonneReponseAGauche = true;
                if (OperationActuelle == Operation.par)
                {
                    Resultat1 = Terme1;
                    Terme1 *= Terme2;
                    if (Terme1 == 1)
                    {
                        Resultat2 = ran.Next(2, 99);
                    }
                    if (Terme1 == 99)
                    {
                        Resultat2 = ran.Next(1, 99);
                    }
                    if (Terme1 > 2 && Terme1 < 98)
                    {
                        int tempR2 = ran.Next(1, Terme1 - 1);
                        int tempR2b = ran.Next(Terme1 + 1, 99);
                        if (ran.Next(0, 1) == 0)
                        {
                            Resultat2 = tempR2;
                        }
                        else
                        {
                            Resultat2 = tempR2b;
                        }
                    }
                }
                else if (OperationActuelle == Operation.plus)
                {
                    Resultat1 = Terme1 + Terme2;
                    int tempR2 = ran.Next(1, Terme1 + Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 + Terme2 + 1, 200);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat2 = tempR2;
                    }
                    else
                    {
                        Resultat2 = tempR2b;
                    }
                }
                else if (OperationActuelle == Operation.moins)
                {
                    Terme1 = ran.Next(3, 99);
                    Terme2 = ran.Next(1, Terme1 - 2);
                    Resultat1 = Terme1 - Terme2;
                    int tempR2 = ran.Next(1, Terme1 - Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 - Terme2 + 1, 99);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat2 = tempR2;
                    }
                    else
                    {
                        Resultat2 = tempR2b;
                    }
                }
                else if (OperationActuelle == Operation.fois)
                {
                    Terme1 = ran.Next(2, 9);
                    Terme2 = ran.Next(2, 9);
                    Resultat1 = Terme1 * Terme2;
                    int tempR2 = ran.Next(1, Terme1 * Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 * Terme2 + 1, 82);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat2 = tempR2;
                    }
                    else
                    {
                        Resultat2 = tempR2b;
                    }
                }
            }
            else
            {
                BonneReponseAGauche = false;
                if (OperationActuelle == Operation.par)
                {
                    Resultat2 = Terme1;
                    Terme1 *= Terme2;
                    if (Terme1 == 1)
                    {
                        Resultat1 = ran.Next(2, 99);
                    }
                    if (Terme1 == 99)
                    {
                        Resultat1 = ran.Next(1, 99);
                    }
                    if (Terme1 > 2 && Terme1 < 98)
                    {
                        int tempR2 = ran.Next(1, Terme1 - 1);
                        int tempR2b = ran.Next(Terme1 + 1, 99);
                        if (ran.Next(0, 1) == 0)
                        {
                            Resultat1 = tempR2;
                        }
                        else
                        {
                            Resultat1 = tempR2b;
                        }
                    }
                }
                else if (OperationActuelle == Operation.plus)
                {
                    Resultat2 = Terme1 + Terme2;
                    int tempR2 = ran.Next(1, Terme1 + Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 + Terme2 + 1, 200);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat1 = tempR2;
                    }
                    else
                    {
                        Resultat1 = tempR2b;
                    }
                }
                else if (OperationActuelle == Operation.moins)
                {
                    Terme1 = ran.Next(3, 99);
                    Terme2 = ran.Next(1, Terme1 - 2);
                    Resultat2 = Terme1 - Terme2;
                    int tempR2 = ran.Next(1, Terme1 - Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 - Terme2 + 1, 99);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat1 = tempR2;
                    }
                    else
                    {
                        Resultat1 = tempR2b;
                    }
                }
                else if (OperationActuelle == Operation.fois)
                {
                    Terme1 = ran.Next(2, 9);
                    Terme2 = ran.Next(2, 9);
                    Resultat2 = Terme1 * Terme2;
                    int tempR2 = ran.Next(1, Terme1 * Terme2 - 1);
                    int tempR2b = ran.Next(Terme1 * Terme2 + 1, 82);
                    if (ran.Next(0, 1) == 0)
                    {
                        Resultat1 = tempR2;
                    }
                    else
                    {
                        Resultat1 = tempR2b;
                    }
                }
            }
        }

        public void GenererTermes()
        {
            if(OperationActuelle == Operation.fois || OperationActuelle == Operation.par)
            {
                Random rnd = new Random();
                Terme1 = rnd.Next(1, 9);
                Terme2 = rnd.Next(1, 9);
            }
            else
            {
                Random rnd = new Random();
                Terme1 = rnd.Next(1, 99);
                Terme2 = rnd.Next(1, 99);
            }
        }

        static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(new Random().Next(v.Length));
        }


    }
}
