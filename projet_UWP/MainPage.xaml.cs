﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace projet_UWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnClickJouer(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(pageJeu));
        }

        private async void OnClickQuitter(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }

        private async void OnClickMeilleursScores(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(pageMeilleursScores));
        }
    }


}
