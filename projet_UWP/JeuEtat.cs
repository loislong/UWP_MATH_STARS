﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace projet_UWP
{
    public class JeuEtat : INotifyPropertyChanged
    {
        private int score;
        private int pointsDeVie;
        

        public JeuEtat()
        {
            Score = 0;
            PointsDeVie = 3;
        }

        public int Score
        {
            get { return this.score; }
            set
            {
                this.score = value;
                this.OnPropertyChanged();

            }
        }
        public int PointsDeVie
        {
            get { return this.pointsDeVie; }
            set
            {
                this.pointsDeVie = value;
                if(this.pointsDeVie == 0)
                {
                    FinDePartie();
                }
                this.OnPropertyChanged();

            }
        }

        private async void FinDePartie()
        {
            ContentDialogfinDePartie fin = new ContentDialogfinDePartie(score);
            await fin.ShowAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
