﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace projet_UWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class pageMeilleursScores : Page
    {


        public pageMeilleursScores()
        {
            this.InitializeComponent();
        }

        public async void OnClickRetour(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame)?.Navigate(typeof(MainPage), null);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            XmlDocument doc = new XmlDocument();

            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile myFile = await storageFolder.CreateFileAsync("dataMScore.xml", Windows.Storage.CreationCollisionOption.OpenIfExists);

            string textFile = await Windows.Storage.FileIO.ReadTextAsync(myFile);
            if (textFile != "")
            {
                doc.LoadXml(textFile);
            }

            XmlNode root = doc.GetElementsByTagName("scores")[0];

            Nom1.Text = root.ChildNodes[0].ChildNodes[0].InnerText;
            Nom2.Text = root.ChildNodes[1].ChildNodes[0].InnerText;
            Nom3.Text = root.ChildNodes[2].ChildNodes[0].InnerText;
            Nom4.Text = root.ChildNodes[3].ChildNodes[0].InnerText;
            Nom5.Text = root.ChildNodes[4].ChildNodes[0].InnerText;
            Nom6.Text = root.ChildNodes[5].ChildNodes[0].InnerText;
            Nom7.Text = root.ChildNodes[6].ChildNodes[0].InnerText;
            Nom8.Text = root.ChildNodes[7].ChildNodes[0].InnerText;
            Nom9.Text = root.ChildNodes[8].ChildNodes[0].InnerText;
            Nom10.Text = root.ChildNodes[9].ChildNodes[0].InnerText;

            Score1.Text = root.ChildNodes[0].ChildNodes[1].InnerText;
            Score2.Text = root.ChildNodes[1].ChildNodes[1].InnerText;
            Score3.Text = root.ChildNodes[2].ChildNodes[1].InnerText;
            Score4.Text = root.ChildNodes[3].ChildNodes[1].InnerText;
            Score5.Text = root.ChildNodes[4].ChildNodes[1].InnerText;
            Score6.Text = root.ChildNodes[5].ChildNodes[1].InnerText;
            Score7.Text = root.ChildNodes[6].ChildNodes[1].InnerText;
            Score8.Text = root.ChildNodes[7].ChildNodes[1].InnerText;
            Score9.Text = root.ChildNodes[8].ChildNodes[1].InnerText;
            Score10.Text = root.ChildNodes[9].ChildNodes[1].InnerText;
        }

        //Checker xml et afficher les meilleurs scores
    }
}
