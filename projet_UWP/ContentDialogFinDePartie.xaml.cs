﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Xml;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Boîte de dialogue de contenu, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace projet_UWP
{
    public sealed partial class ContentDialogfinDePartie : ContentDialog
    {
        private int score;

        public ContentDialogfinDePartie()
        {
            this.InitializeComponent();
        }

        public ContentDialogfinDePartie(int score)
        {
            this.InitializeComponent();
            this.Score = score;
        }

        public int Score { get => score; set => score = value; }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //rejouer
            (Window.Current.Content as Frame)?.Navigate(typeof(pageJeu), null);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            //quitter
            (Window.Current.Content as Frame)?.Navigate(typeof(MainPage), null);
        }

        
        private async void SauvegarderScore(object sender, RoutedEventArgs e)
        {
            XmlDocument doc = new XmlDocument();

          //  string xmlActuel = File.ReadAllText("dataMScore.xml");
          //  doc.LoadXml(xmlActuel);

            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile myFile = await storageFolder.CreateFileAsync("dataMScore.xml", Windows.Storage.CreationCollisionOption.OpenIfExists);

            string textFile = await Windows.Storage.FileIO.ReadTextAsync(myFile);
            if (textFile != "")
            {
                doc.LoadXml(textFile);
            }

            if (!doc.HasChildNodes)
            {
                doc.LoadXml("<scores>< first >< nom ></ nom >< score ></ score ></ first >< second >< nom ></ nom >< score ></ score ></ second >< third >< nom ></ nom >< score ></ score ></ third >< fourth >< nom ></ nom >< score ></ score ></ fourth >< fifth >< nom ></ nom >< score ></ score ></ fifth >< sixth >< nom ></ nom >< score ></ score ></ sixth >< seventh >< nom ></ nom >< score ></ score ></ seventh >< eighth >< nom ></ nom >< score ></ score ></ eighth >< ninth >< nom ></ nom >< score ></ score ></ ninth >< tenth >< nom ></ nom >< score ></ score ></ tenth ></ scores >");
            }

            //XmlElement nouveauNom = doc.CreateElement("nom");
            //nouveauNom.InnerText = nomJoueur.Text;

            //XmlElement nouveauScore = doc.CreateElement("score");
            //nouveauScore.InnerText = this.Score.ToString();

            XmlNode root = doc.GetElementsByTagName("scores")[0];

            string nomTemp;
            string scoreTemp;

            string nomTemp2;
            string scoreTemp2;

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes[i].ChildNodes[1].InnerText != "" && Convert.ToInt32(root.ChildNodes[i].ChildNodes[1].InnerText) < Score)
                {
                    scoreTemp = root.ChildNodes[i].ChildNodes[1].InnerText;
                    nomTemp = root.ChildNodes[i].ChildNodes[0].InnerText;

                    root.ChildNodes[i].ChildNodes[1].InnerText = this.Score.ToString();
                    root.ChildNodes[i].ChildNodes[0].InnerText = nomJoueur.Text;

                    if (i < root.ChildNodes.Count)
                    {
                        for (int j = i + 1; j < root.ChildNodes.Count; j++)
                        {
                            scoreTemp2 = root.ChildNodes[j].ChildNodes[1].InnerText;
                            nomTemp2 = root.ChildNodes[j].ChildNodes[0].InnerText;

                            root.ChildNodes[i].ChildNodes[1].InnerText = scoreTemp;
                            root.ChildNodes[i].ChildNodes[0].InnerText = nomTemp;

                            scoreTemp = scoreTemp2;
                            nomTemp = nomTemp2;
                        }
                    }
                    break;
                }
                else if (root.ChildNodes[i].ChildNodes[1].InnerText == "")
                {
                    root.ChildNodes[i].ChildNodes[1].InnerText = this.Score.ToString();
                    root.ChildNodes[i].ChildNodes[0].InnerText = nomJoueur.Text;
                    break;
                }
            }
            await Windows.Storage.FileIO.WriteTextAsync(myFile, doc.InnerXml);
        }
    }
}
