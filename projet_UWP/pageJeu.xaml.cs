﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace projet_UWP
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class pageJeu : Page
    {
        public Jeu monJeu { get; set; }
        public JeuEtat monEtat { get; set; }

        public pageJeu()
        {
            monEtat = new JeuEtat();
            this.InitializeComponent();
            this.Rejouer();
        }

        public void Rejouer()
        {
            this.monJeu = new Jeu();
            monJeu.GenererOperation();
            monJeu.GenererTermes();
            monJeu.GenererResultats();
        }

        private async void OnClickLeft(object sender, RoutedEventArgs e)
        {
            increment = 10;
            if (monJeu.BonneReponseAGauche)
            {
                monEtat.Score++;
            }
            else
            {
                monEtat.PointsDeVie--;
            }
            if (monEtat.PointsDeVie >= 0)
            {
                monJeu.GenererOperation();
                monJeu.GenererTermes();
                monJeu.GenererResultats();
            }
        }

        private async void OnClickRight(object sender, RoutedEventArgs e)
        {
            increment = 10;
            if (monJeu.BonneReponseAGauche)
            {
                monEtat.PointsDeVie--;
            }
            else
            {
                monEtat.Score++;
            }
            if (monEtat.PointsDeVie >= 0)
            {
                monJeu.GenererTermes();
                monJeu.GenererResultats();
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(1);
            dt.Tick += Dt_Tick;
            dt.Start();
        }

        private int increment = 10;
        private void Dt_Tick(object sender, object e)
        {
            increment--;
            if (increment < 0)
            {
                increment = 10;
                monEtat.PointsDeVie--;
                if (monEtat.PointsDeVie >= 0)
                {
                    monJeu.GenererTermes();
                    monJeu.GenererResultats();
                }
                else
                {

                }
            }
            TimerLabel.Text = increment.ToString();

        }
    }

    public enum Operation
    {
        fois, plus, moins, par
    };
}
